import React from "react";
import NestedPostsButton from "./components/NestedPostsButton";

class BBImprovementsPlugin {
  initialize(registry, store) {
    this.injectCustomStyles()

    registry.registerChannelHeaderButtonAction(
      NestedPostsButton,
      () => {},
      "Toggle nested posts in main view"
    );
  }

  injectCustomStyles() {
    var css = global.document.createElement("style");
    css.type = "text/css";
    css.innerHTML = '[data-nested-posts="true"] #post-list .post--comment { display: none; } .icon.icon--wide { padding: 0 8px; }';
    global.document.body.appendChild(css);
  }

  setupBodyAttributes() {}
}

window.registerPlugin("com.bluebrick.bb-improvements", new BBImprovementsPlugin());

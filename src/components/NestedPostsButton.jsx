import React from "react";
import { connect } from "react-redux";
import { getTheme } from "mattermost-redux/selectors/entities/preferences";
import { getMyPreferences } from "mattermost-redux/selectors/entities/preferences";
import { getCurrentUserId } from "mattermost-redux/selectors/entities/users";
import { savePreferences } from "mattermost-redux/actions/preferences";
import Preferences from "mattermost-redux/constants/preferences";
import MinimizeIcon from "../images/minimize.svg";
import MaximizeIcon from "../images/maximize.svg";

const mapStateToProps = (state) => {
  return {
    userId: getCurrentUserId(state),
    preferences: getMyPreferences(state),
    theme: getTheme(state),
  };
};

const mapDispatchToProps = { savePreferences };

class NestedPostsButton extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      enabled: false,
    };

    this.preferencesName = "bbi_nested_posts";

    this.refIcon = undefined;
    this.toggle = this.toggle.bind(this);
  }

  componentDidMount() {
    this.loadPrefences();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.enabled === false && this.state.enabled === true)
      this.enable();
    if (prevState.enabled === true && this.state.enabled === false)
      this.disable();
  }

  toggle() {
    const enabled = !this.state.enabled;
    this.setState({ enabled });
    this.props.savePreferences(this.props.userId, [
      {
        user_id: this.props.userId,
        category: Preferences.CATEGORY_DISPLAY_SETTINGS,
        name: this.preferencesName,
        value: enabled.toString(),
      },
    ]);
  }

  enable() {
    document.body.setAttribute("data-nested-posts", true);
    this.refIcon.parentNode.classList.add("active");
  }

  disable() {
    document.body.removeAttribute("data-nested-posts");
    this.refIcon.parentNode.classList.remove("active");
  }

  loadPrefences() {
    const pref = Object.values(this.props.preferences).find(
      (pref) =>
        pref.user_id === this.props.userId &&
        pref.category === Preferences.CATEGORY_DISPLAY_SETTINGS &&
        pref.name === this.preferencesName
    );
    this.setState({
      enabled: pref && pref.value === "true",
    });
  }

  render() {
    return (
      <span
        onClick={this.toggle}
        ref={(el) => (this.refIcon = el)}
        class="icon icon--wide"
      >
        {this.state.enabled ? <MinimizeIcon /> : <MaximizeIcon />}
      </span>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NestedPostsButton);
